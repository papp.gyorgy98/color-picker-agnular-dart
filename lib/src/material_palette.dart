/**
 * https://github.com/shuhei/material-colors/blob/master/dist/colors.es2015.js
 */

import 'package:angular/angular.dart';

enum MaterialColors{
  RED,
  PINK,
  PURPLE,
  DEEPPURPLE,
  INDIGO,
  BLUE,
  LIGHTBLUE,
  CYAN,
  TEAL,
  GREEN,
  LIGHTGREEN,
  LIME,
  YELLOW,
  AMBER,
  ORANGE,
  DEEPORANGE,
  BROWN,
  GREY,
  BLUEGREY
}

enum ColorsValue{
  V50,
  V100,
  V200,
  V300,
  V400,
  V500,
  V600,
  V700,
  V800,
  V900,
  A100,
  A200,
  A400,
  A700

}

@Injectable()
class MaterialPalette{

  Map<MaterialColors, Map<ColorsValue, String>> _colorsList;
  List<String> colorNames;
  List<String> valuesName;

  MaterialPalette(){
    this._colorsList = new Map();
    this._colorsList[MaterialColors.RED] = red;
    this._colorsList[MaterialColors.PINK] = pink;
    this._colorsList[MaterialColors.PURPLE] = purple;
    this._colorsList[MaterialColors.DEEPPURPLE] = deepPurple;
    this._colorsList[MaterialColors.INDIGO] = indigo;
    this._colorsList[MaterialColors.BLUE] = blue;
    this._colorsList[MaterialColors.LIGHTBLUE] = lightBlue;
    this._colorsList[MaterialColors.CYAN] = cyan;
    this._colorsList[MaterialColors.TEAL] = teal;
    this._colorsList[MaterialColors.GREEN] = green;
    this._colorsList[MaterialColors.LIGHTGREEN] = lightGreen;
    this._colorsList[MaterialColors.LIME] = lime;
    this._colorsList[MaterialColors.YELLOW] = yellow;
    this._colorsList[MaterialColors.AMBER] = amber;
    this._colorsList[MaterialColors.ORANGE] = orange;
    this._colorsList[MaterialColors.DEEPORANGE] = deepOrange;
    this._colorsList[MaterialColors.BROWN] = brown;
    this._colorsList[MaterialColors.GREY] = grey;
    this._colorsList[MaterialColors.BLUEGREY] = blueGrey;

    this.colorNames = [
      "Red",
      "Pink",
      "Purple",
      "Deep Purple",
      "Indigo",
      "Blue",
      "Light Blue",
      "Cyan",
      "Teal",
      "Green",
      "Light Green",
      "Lime",
      "Yellow",
      "Amber",
      "Orange",
      "Deep Orange",
      "Brown",
      "Grey",
      "Blue Grey"
    ];

    this.valuesName = [
      "50",
      "100",
      "200",
      "300",
      "400",
      "500",
      "600",
      "700",
      "800",
      "900",
      "A100",
      "A200",
      "A400",
      "A700"
    ];
  }

  String getColor(MaterialColors color, ColorsValue value){
    var retVal = this._colorsList[color][value];
    return retVal.isEmpty ? "" : retVal;
  }

  Map<ColorsValue, String> getAllColor(MaterialColors color){
    return this._colorsList[color];
  }

  String getColorWithMiddleValue(MaterialColors color){
    return this.getColor(color, ColorsValue.V500);
  }

  List<ColorsValue> getColorsAvailableValues(MaterialColors color){
    return this._colorsList[color].keys.toList(growable: false);
  }

  String getValueName(ColorsValue value){
    return this.valuesName[value.index];
  }

  String getColorName(MaterialColors color){
    return this.colorNames[color.index];
  }

  final Map<ColorsValue, String> red = {ColorsValue.V50:"#ffebee",ColorsValue.V100:"#ffcdd2",ColorsValue.V200:"#ef9a9a",ColorsValue.V300:"#e57373",ColorsValue.V400:"#ef5350",ColorsValue.V500:"#f44336",ColorsValue.V600:"#e53935",ColorsValue.V700:"#d32f2f",ColorsValue.V800:"#c62828",ColorsValue.V900:"#b71c1c",ColorsValue.A100:"#ff8a80",ColorsValue.A200:"#ff5252",ColorsValue.A400:"#ff1744",ColorsValue.A700:"#d50000"};
  final Map<ColorsValue, String> pink = {ColorsValue.V50:"#fce4ec",ColorsValue.V100:"#f8bbd0",ColorsValue.V200:"#f48fb1",ColorsValue.V300:"#f06292",ColorsValue.V400:"#ec407a",ColorsValue.V500:"#e91e63",ColorsValue.V600:"#d81b60",ColorsValue.V700:"#c2185b",ColorsValue.V800:"#ad1457",ColorsValue.V900:"#880e4f",ColorsValue.A100:"#ff80ab",ColorsValue.A200:"#ff4081",ColorsValue.A400:"#f50057",ColorsValue.A700:"#c51162"};
  final Map<ColorsValue, String> purple = {ColorsValue.V50:"#f3e5f5",ColorsValue.V100:"#e1bee7",ColorsValue.V200:"#ce93d8",ColorsValue.V300:"#ba68c8",ColorsValue.V400:"#ab47bc",ColorsValue.V500:"#9c27b0",ColorsValue.V600:"#8e24aa",ColorsValue.V700:"#7b1fa2",ColorsValue.V800:"#6a1b9a",ColorsValue.V900:"#4a148c",ColorsValue.A100:"#ea80fc",ColorsValue.A200:"#e040fb",ColorsValue.A400:"#d500f9",ColorsValue.A700:"#aa00ff"};
  final Map<ColorsValue, String> deepPurple = {ColorsValue.V50:"#ede7f6",ColorsValue.V100:"#d1c4e9",ColorsValue.V200:"#b39ddb",ColorsValue.V300:"#9575cd",ColorsValue.V400:"#7e57c2",ColorsValue.V500:"#673ab7",ColorsValue.V600:"#5e35b1",ColorsValue.V700:"#512da8",ColorsValue.V800:"#4527a0",ColorsValue.V900:"#311b92",ColorsValue.A100:"#b388ff",ColorsValue.A200:"#7c4dff",ColorsValue.A400:"#651fff",ColorsValue.A700:"#6200ea"};
  final Map<ColorsValue, String> indigo = {ColorsValue.V50:"#e8eaf6",ColorsValue.V100:"#c5cae9",ColorsValue.V200:"#9fa8da",ColorsValue.V300:"#7986cb",ColorsValue.V400:"#5c6bc0",ColorsValue.V500:"#3f51b5",ColorsValue.V600:"#3949ab",ColorsValue.V700:"#303f9f",ColorsValue.V800:"#283593",ColorsValue.V900:"#1a237e",ColorsValue.A100:"#8c9eff",ColorsValue.A200:"#536dfe",ColorsValue.A400:"#3d5afe",ColorsValue.A700:"#304ffe"};
  final Map<ColorsValue, String> blue = {ColorsValue.V50:"#e3f2fd",ColorsValue.V100:"#bbdefb",ColorsValue.V200:"#90caf9",ColorsValue.V300:"#64b5f6",ColorsValue.V400:"#42a5f5",ColorsValue.V500:"#2196f3",ColorsValue.V600:"#1e88e5",ColorsValue.V700:"#1976d2",ColorsValue.V800:"#1565c0",ColorsValue.V900:"#0d47a1",ColorsValue.A100:"#82b1ff",ColorsValue.A200:"#448aff",ColorsValue.A400:"#2979ff",ColorsValue.A700:"#2962ff"};
  final Map<ColorsValue, String> lightBlue = {ColorsValue.V50:"#e1f5fe",ColorsValue.V100:"#b3e5fc",ColorsValue.V200:"#81d4fa",ColorsValue.V300:"#4fc3f7",ColorsValue.V400:"#29b6f6",ColorsValue.V500:"#03a9f4",ColorsValue.V600:"#039be5",ColorsValue.V700:"#0288d1",ColorsValue.V800:"#0277bd",ColorsValue.V900:"#01579b",ColorsValue.A100:"#80d8ff",ColorsValue.A200:"#40c4ff",ColorsValue.A400:"#00b0ff",ColorsValue.A700:"#0091ea"};
  final Map<ColorsValue, String> cyan = {ColorsValue.V50:"#e0f7fa",ColorsValue.V100:"#b2ebf2",ColorsValue.V200:"#80deea",ColorsValue.V300:"#4dd0e1",ColorsValue.V400:"#26c6da",ColorsValue.V500:"#00bcd4",ColorsValue.V600:"#00acc1",ColorsValue.V700:"#0097a7",ColorsValue.V800:"#00838f",ColorsValue.V900:"#006064",ColorsValue.A100:"#84ffff",ColorsValue.A200:"#18ffff",ColorsValue.A400:"#00e5ff",ColorsValue.A700:"#00b8d4"};
  final Map<ColorsValue, String> teal = {ColorsValue.V50:"#e0f2f1",ColorsValue.V100:"#b2dfdb",ColorsValue.V200:"#80cbc4",ColorsValue.V300:"#4db6ac",ColorsValue.V400:"#26a69a",ColorsValue.V500:"#009688",ColorsValue.V600:"#00897b",ColorsValue.V700:"#00796b",ColorsValue.V800:"#00695c",ColorsValue.V900:"#004d40",ColorsValue.A100:"#a7ffeb",ColorsValue.A200:"#64ffda",ColorsValue.A400:"#1de9b6",ColorsValue.A700:"#00bfa5"};
  final Map<ColorsValue, String> green = {ColorsValue.V50:"#e8f5e9",ColorsValue.V100:"#c8e6c9",ColorsValue.V200:"#a5d6a7",ColorsValue.V300:"#81c784",ColorsValue.V400:"#66bb6a",ColorsValue.V500:"#4caf50",ColorsValue.V600:"#43a047",ColorsValue.V700:"#388e3c",ColorsValue.V800:"#2e7d32",ColorsValue.V900:"#1b5e20",ColorsValue.A100:"#b9f6ca",ColorsValue.A200:"#69f0ae",ColorsValue.A400:"#00e676",ColorsValue.A700:"#00c853"};
  final Map<ColorsValue, String> lightGreen = {ColorsValue.V50:"#f1f8e9",ColorsValue.V100:"#dcedc8",ColorsValue.V200:"#c5e1a5",ColorsValue.V300:"#aed581",ColorsValue.V400:"#9ccc65",ColorsValue.V500:"#8bc34a",ColorsValue.V600:"#7cb342",ColorsValue.V700:"#689f38",ColorsValue.V800:"#558b2f",ColorsValue.V900:"#33691e",ColorsValue.A100:"#ccff90",ColorsValue.A200:"#b2ff59",ColorsValue.A400:"#76ff03",ColorsValue.A700:"#64dd17"};
  final Map<ColorsValue, String> lime = {ColorsValue.V50:"#f9fbe7",ColorsValue.V100:"#f0f4c3",ColorsValue.V200:"#e6ee9c",ColorsValue.V300:"#dce775",ColorsValue.V400:"#d4e157",ColorsValue.V500:"#cddc39",ColorsValue.V600:"#c0ca33",ColorsValue.V700:"#afb42b",ColorsValue.V800:"#9e9d24",ColorsValue.V900:"#827717",ColorsValue.A100:"#f4ff81",ColorsValue.A200:"#eeff41",ColorsValue.A400:"#c6ff00",ColorsValue.A700:"#aeea00"};
  final Map<ColorsValue, String> yellow = {ColorsValue.V50:"#fffde7",ColorsValue.V100:"#fff9c4",ColorsValue.V200:"#fff59d",ColorsValue.V300:"#fff176",ColorsValue.V400:"#ffee58",ColorsValue.V500:"#ffeb3b",ColorsValue.V600:"#fdd835",ColorsValue.V700:"#fbc02d",ColorsValue.V800:"#f9a825",ColorsValue.V900:"#f57f17",ColorsValue.A100:"#ffff8d",ColorsValue.A200:"#ffff00",ColorsValue.A400:"#ffea00",ColorsValue.A700:"#ffd600"};
  final Map<ColorsValue, String> amber = {ColorsValue.V50:"#fff8e1",ColorsValue.V100:"#ffecb3",ColorsValue.V200:"#ffe082",ColorsValue.V300:"#ffd54f",ColorsValue.V400:"#ffca28",ColorsValue.V500:"#ffc107",ColorsValue.V600:"#ffb300",ColorsValue.V700:"#ffa000",ColorsValue.V800:"#ff8f00",ColorsValue.V900:"#ff6f00",ColorsValue.A100:"#ffe57f",ColorsValue.A200:"#ffd740",ColorsValue.A400:"#ffc400",ColorsValue.A700:"#ffab00"};
  final Map<ColorsValue, String> orange = {ColorsValue.V50:"#fff3e0",ColorsValue.V100:"#ffe0b2",ColorsValue.V200:"#ffcc80",ColorsValue.V300:"#ffb74d",ColorsValue.V400:"#ffa726",ColorsValue.V500:"#ff9800",ColorsValue.V600:"#fb8c00",ColorsValue.V700:"#f57c00",ColorsValue.V800:"#ef6c00",ColorsValue.V900:"#e65100",ColorsValue.A100:"#ffd180",ColorsValue.A200:"#ffab40",ColorsValue.A400:"#ff9100",ColorsValue.A700:"#ff6d00"};
  final Map<ColorsValue, String> deepOrange = {ColorsValue.V50:"#fbe9e7",ColorsValue.V100:"#ffccbc",ColorsValue.V200:"#ffab91",ColorsValue.V300:"#ff8a65",ColorsValue.V400:"#ff7043",ColorsValue.V500:"#ff5722",ColorsValue.V600:"#f4511e",ColorsValue.V700:"#e64a19",ColorsValue.V800:"#d84315",ColorsValue.V900:"#bf360c",ColorsValue.A100:"#ff9e80",ColorsValue.A200:"#ff6e40",ColorsValue.A400:"#ff3d00",ColorsValue.A700:"#dd2c00"};
  final Map<ColorsValue, String> brown = {ColorsValue.V50:"#efebe9",ColorsValue.V100:"#d7ccc8",ColorsValue.V200:"#bcaaa4",ColorsValue.V300:"#a1887f",ColorsValue.V400:"#8d6e63",ColorsValue.V500:"#795548",ColorsValue.V600:"#6d4c41",ColorsValue.V700:"#5d4037",ColorsValue.V800:"#4e342e",ColorsValue.V900:"#3e2723"};
  final Map<ColorsValue, String> grey = {ColorsValue.V50:"#fafafa",ColorsValue.V100:"#f5f5f5",ColorsValue.V200:"#eeeeee",ColorsValue.V300:"#e0e0e0",ColorsValue.V400:"#bdbdbd",ColorsValue.V500:"#9e9e9e",ColorsValue.V600:"#757575",ColorsValue.V700:"#616161",ColorsValue.V800:"#424242",ColorsValue.V900:"#212121"};
  final Map<ColorsValue, String> blueGrey = {ColorsValue.V50:"#eceff1",ColorsValue.V100:"#cfd8dc",ColorsValue.V200:"#b0bec5",ColorsValue.V300:"#90a4ae",ColorsValue.V400:"#78909c",ColorsValue.V500:"#607d8b",ColorsValue.V600:"#546e7a",ColorsValue.V700:"#455a64",ColorsValue.V800:"#37474f",ColorsValue.V900:"#263238"};
}