import 'dart:async';

import 'dart:html';
import 'dart:math';
import 'package:angular/angular.dart';
import 'package:angular/core.dart';
import 'package:quiver/async.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/laminate/components/modal/modal.dart';
import 'package:angular_components/laminate/overlay/module.dart';
import 'package:angular_components/material_tab/tab_change_event.dart';
import 'package:angular_components/utils/browser/dom_service/angular_2.dart';
import 'package:angular_components/utils/browser/window/module.dart';
import 'package:vector_math/vector_math.dart';

import '../material_palette.dart';

@Component(
  selector: 'color-picker',
  styleUrls: ['color_picker.css'],
  templateUrl: 'color_picker_component.html',
  directives: [
    AutoDismissDirective,
    AutoFocusDirective,
    MaterialIconComponent,
    materialInputDirectives,
    MaterialInputComponent,
    MaterialButtonComponent,
    MaterialTooltipDirective,
    MaterialDialogComponent,
    MaterialListComponent,
    MaterialListItemComponent,
    MaterialSelectItemComponent,
    MaterialSliderComponent,
    ModalComponent,
    MaterialToggleComponent,
    FixedMaterialTabStripComponent,
    NgFor,
    NgIf,
    NgSwitch,
    NgSwitchDefault,
    NgSwitchWhen,
    NgClass
  ],
  providers: [
    overlayBindings,
    domServiceBinding,
    rtlProvider,
    windowBindings,
    MaterialPalette,
  ],
)
class ColorPickerComponent implements OnInit, AfterViewInit, AfterViewChecked {
  final MaterialPalette palette;
  final List<MaterialColors> listOfMaterialColors = MaterialColors.values;
  String selectedColorInputTab = "HEX";
  String selectedColorTab = "HSL";
  Vector4 selectedColorRGB;
  Vector4 selectedColorHSV;
  Vector4 selectedHueColor;
  bool gradientSetupWasCompleted = false;
  Map<String, Vector4> colorHistory;
  String _lastlySelectedColorString = "";

  double canvasHeight = 230.0;
  double canvasWidthSquare = 230.0;
  double canvasWidthRectangle = 40.0;

  String get selectedColorString {
    switch (this.selectedColorInputTab) {
      case "HEX":
        return "#${toHexString(this.selectedColorRGB, alpha: true)}";
        break;
      case "RGBA":
        return this.colorToRGBAString(this.selectedColorRGB);
        break;
      case "HSLA":
        var hslColor = new Vector4.zero();
        Colors.rgbToHsl(this.selectedColorRGB, hslColor);
        return "hsla(${(hslColor.r * 360.0).toStringAsFixed(1)}, ${(hslColor.g *
            100).toStringAsFixed(1)}%, ${(hslColor.b * 100).toStringAsFixed(
            1)}%, ${(hslColor.a * 100).toStringAsFixed(1)})";
        break;
    }
    return "error";
  }

  set selectedColorString(String newValue) {
    switch (this.selectedColorInputTab) {
      case "HEX":
        break;
      case "RGBA":
        break;
      case "HSLA":
        break;
    }

  }

  String get lastlySelectedColorString{
    return this._lastlySelectedColorString;
  }

  set lastlySelectedColorString(String newValue) {
    RegExp rgbaBegin = new RegExp("rgb[a]?[(][0-9]*[.[0-9]*]?,[ ]*[0-9]*[.[0-9]*]?,[ ]*[0-9]*[.[0-9]*]?[ ]*[)]?\$");
    RegExp hex = new RegExp("#[0-9a-fA-F]{3,8}*\$");
    RegExp hsvBegin = new RegExp("hsva[a]?[(][0-9]*[.[0-9]*]?,[ ]*[0-9]*[.[0-9]*]?,[ ]*[0-9]*[.[0-9]*]?[ ]*[)]?\$");

    if(hex.hasMatch(newValue)){

    } else if(rgbaBegin.hasMatch(newValue)){
      var numbers = newValue
          .replaceAll(" ", "")
          .replaceAll(")", "")
          .substring(4, newValue.indexOf(")",5));
      List<String> listOfNumber = numbers.split(",");
      if(listOfNumber.length > 2){
        try {
          this.selectedColorRGB = new Vector4(
              num.parse(listOfNumber[0]) / 255.0,
              num.parse(listOfNumber[1]) / 255.0,
              num.parse(listOfNumber[2]) / 255.0,
              listOfNumber.length > 3 ? num.parse(listOfNumber[3]) : 1.0);
        }catch(error){
          print("error");
        }
      }
    } else if(hsvBegin.hasMatch(newValue)){
      var numbers = newValue
          .replaceAll(" ", "")
          .replaceAll(")", "")
          .substring(4, newValue.indexOf(")",5));
      List<String> listOfNumber = numbers.split(",");
      if(listOfNumber.length > 2){
        try {
          Colors.hsvToRgb(new Vector4(
              num.parse(listOfNumber[0]) / 360.0,
              num.parse(listOfNumber[1]) / 100.0,
              num.parse(listOfNumber[2]) / 100.0,
              listOfNumber.length > 3 ? num.parse(listOfNumber[3]) : 1.0), this.selectedColorRGB);
        }catch(error){
          print("error");
        }
      }
    }
  }
  
  int get selectedColorR => (this.selectedColorRGB.r * 255).floor();
  int get selectedColorG => (this.selectedColorRGB.g * 255).floor();
  int get selectedColorB => (this.selectedColorRGB.b * 255).floor();

  void changeRGB(num value, int type){
    switch(type){
      case 0:
          this.selectedColorRGB.r = (value / 255).toDouble();
        break;
      case 1:
          this.selectedColorRGB.g = (value / 255).toDouble();
        break;
      case 2:
          this.selectedColorRGB.b = (value / 255).toDouble();
        break;
    }
  }

  final tabLabelsInput = const <String>[
    'HEX',
    'RGBA',
    'HSLA'
  ];

  final tabLabelsEditor = const <String>[
    'HSL',
    'RGB',
    'Grid',
    'List',
    'History'
  ];

  @ViewChild("colorButtonInput") MaterialInputComponent colorButtonInput;

  dynamic canvasCtxSV;
  dynamic canvasCtxSVMouse;
  dynamic canvasCtxH;
  dynamic canvasCtxA;
  dynamic canvasCtxHMouse;
  dynamic canvasCtxAMouse;

  @Input()
  bool visible = true;

  final _visibleStream = new StreamController<bool>();

  @Output() Stream<bool> get visibleChange => _visibleStream.stream;

  ColorPickerComponent(this.palette) {
    this.selectedColorRGB = Colors.red;
    this.selectedColorHSV = new Vector4.zero();
    Colors.rgbToHsv(this.selectedColorRGB, this.selectedColorHSV);
    this.selectedHueColor = Colors.red;

    this.colorHistory = new Map();
  }

  Vector4 getSelectedColorCoordinates(){
    Colors.rgbToHsv(this.selectedColorRGB, this.selectedColorHSV);
    return new Vector4(
        this.selectedColorHSV.g * this.canvasWidthSquare,
        (1 - this.selectedColorHSV.b) * this.canvasHeight,
        this.selectedColorHSV.r * this.canvasHeight,
        (1 - this.selectedColorHSV.a) * this.canvasHeight
    );
  }

  void setupGradient() {
    this.canvasCtxH.rect(0, 0, this.canvasWidthRectangle, this.canvasHeight);
    var grd1 = this.canvasCtxH.createLinearGradient(0, 0, 0, this.canvasHeight);
    var helper = 1 / 6;
    grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
    grd1.addColorStop(helper, 'rgba(255, 255, 0, 1)');
    grd1.addColorStop(helper * 2, 'rgba(0, 255, 0, 1)');
    grd1.addColorStop(helper * 3, 'rgba(0, 255, 255, 1)');
    grd1.addColorStop(helper * 4, 'rgba(0, 0, 255, 1)');
    grd1.addColorStop(helper * 5, 'rgba(255, 0, 255, 1)');
    grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
    this.canvasCtxH.fillStyle = grd1;
    this.canvasCtxH.fill();

    this.canvasCtxA.fillStyle = "rgba(220,220,220, 1.0)";
    this.canvasCtxA.fillRect(
        0, 0, this.canvasWidthRectangle, this.canvasHeight);
    this.canvasCtxA.fill();

    this.canvasCtxA.fillStyle = "rgba(150,150,150, 1.0)";

    double colNum = 10.5;

    double size = (this.canvasWidthRectangle / colNum).floor().toDouble();

    for (int j = 0; j < ((this.canvasHeight / size).floor() + 1); j ++) {
      for (int i = 0; i < colNum; i++) {
        this.canvasCtxA.rect(
            i * (size * 2),
            j * (size * 2),
            //0,
            size, size);
        this.canvasCtxA.rect(
            i * (size * 2) + size,
            j * (size * 2) + size,
            //size,
            size, size);
      }
    }

    this.canvasCtxA.fill();

    var gradient = this.canvasCtxA.createLinearGradient(
        0, 0, 0, this.canvasHeight);
    gradient.addColorStop(0, 'rgba(255, 0, 0, 1.0)');
    gradient.addColorStop(1, 'rgba(255, 0, 0, 0)');
    this.canvasCtxA.fillStyle = gradient;
    this.canvasCtxA.fillRect(
        0, 0, this.canvasWidthRectangle, this.canvasHeight);

    Vector4 selectedColorCoordinates = this.getSelectedColorCoordinates();

    this.canvasCtxSVMouse.clearRect(
        0,0, this.canvasWidthSquare, this.canvasHeight);
    this.canvasCtxSVMouse.beginPath();
    this.canvasCtxSVMouse.arc(selectedColorCoordinates.x, selectedColorCoordinates.y, 6, 0, 2 * pi);
    this.canvasCtxSVMouse.strokeStyle = "black";
    this.canvasCtxSVMouse.lineWidth = 3;
    this.canvasCtxSVMouse.stroke();

    this.canvasCtxSVMouse.beginPath();
    this.canvasCtxSVMouse.arc(selectedColorCoordinates.x, selectedColorCoordinates.y, 6, 0, 2 * pi);
    this.canvasCtxSVMouse.strokeStyle = "white";
    this.canvasCtxSVMouse.lineWidth = 2;
    this.canvasCtxSVMouse.stroke();

    this.canvasCtxHMouse.clearRect(
        0, 0, this.canvasWidthRectangle, this.canvasHeight);
    this.canvasCtxHMouse.fillStyle = "white";
    this.canvasCtxHMouse.fillRect(
        0, selectedColorCoordinates.z - 2, this.canvasWidthRectangle, 2);
    this.canvasCtxHMouse.fillRect(
        0, selectedColorCoordinates.z + 2, this.canvasWidthRectangle, 2);

    this.canvasCtxAMouse.clearRect(
        0, 0, this.canvasWidthRectangle, this.canvasHeight);
    this.canvasCtxAMouse.fillStyle = "white";
    this.canvasCtxAMouse.fillRect(
        0, selectedColorCoordinates.w - 2, this.canvasWidthRectangle, 2);
    this.canvasCtxAMouse.fillRect(
        0, selectedColorCoordinates.w + 2, this.canvasWidthRectangle, 2);

    var imageData = (this.canvasCtxH.getImageData(
        20, selectedColorCoordinates.z.floor(), 1, 1) as ImageData).data;
    this.selectedHueColor = new Vector4(
        imageData[0] / 255,
        imageData[1] / 255,
        imageData[2] / 255,
        imageData[3] / 255);

    updateGradientMap();

    this.gradientSetupWasCompleted = true;
  }

  String colorToRGBAString(Vector4 color) {
    return 'rgba(${(color.r * 255).toStringAsFixed(1)}, ${(color.g * 255)
        .toStringAsFixed(1)}, ${(color.b * 255).toStringAsFixed(1)}, ${(color.a)
        .toStringAsFixed(1)})';
  }

  String colorWithAlphaToRGBAString(Vector3 color, double alpha) {
    return 'rgba(${color.r * 255}, ${color.g * 255}, ${color.b *
        255}, ${alpha})';
  }

  void updateGradientMap() {
    this.canvasCtxSV.fillStyle =
        this.colorWithAlphaToRGBAString(this.selectedHueColor.rgb, 1.0);
    this.canvasCtxSV.fillRect(0, 0, this.canvasWidthSquare, this.canvasHeight);

    var grd = this.canvasCtxSV.createLinearGradient(
        0, 0, this.canvasWidthSquare, 0);
    grd.addColorStop(0, 'rgba(255, 255, 255, 1)');
    grd.addColorStop(1, 'rgba(255, 255, 255, 0)');

    this.canvasCtxSV.fillStyle = grd;
    this.canvasCtxSV.fillRect(0, 0, this.canvasWidthSquare, this.canvasHeight);

    var grd2 = this.canvasCtxSV.createLinearGradient(
        0, 0, 0, this.canvasHeight);
    grd2.addColorStop(0, 'rgba(0, 0, 0, 0)');
    grd2.addColorStop(1, 'rgba(0, 0, 0, 1)');

    this.canvasCtxSV.fillStyle = grd2;
    this.canvasCtxSV.fillRect(0, 0, this.canvasWidthSquare, this.canvasHeight);
  }

  String get color {
    return "rgba(${this.selectedColorRGB.r * 255}, ${this.selectedColorRGB.g *
        255}, ${this.selectedColorRGB.b * 255}, ${this.selectedColorRGB.a})";
  }

  String get color2 {
    return "#${Colors.toHexString(this.selectedHueColor, alpha: true)}";
  }

  void selectMaterialColor(MaterialColors color, ColorsValue value){
    Colors.fromHexString(this.palette.getColor(color, value), this.selectedColorRGB);
    Colors.rgbToHsv(this.selectedColorRGB, this.selectedColorHSV);
  }

  void selectPreviousColor(String colorHexString){
    this.selectedColorRGB = this.colorHistory[colorHexString];
    Colors.rgbToHsv(this.selectedColorRGB, this.selectedColorHSV);
  }

  bool enableMouseTrack = false;
  int selectedGradient = 0;

  void selectColorFromGradient(MouseEvent event) {
    if (enableMouseTrack) {
      var offset = event.offset;
      print("x: ${offset.x} - y: ${offset.y}");
      switch (this.selectedGradient) {
        case 0:
          this.canvasCtxSVMouse.clearRect(
              0, 0, this.canvasWidthSquare, this.canvasHeight);
          this.canvasCtxSVMouse.beginPath();
          this.canvasCtxSVMouse.arc(offset.x, offset.y, 6, 0, 2 * pi);
          this.canvasCtxSVMouse.strokeStyle = "black";
          this.canvasCtxSVMouse.lineWidth = 3;
          this.canvasCtxSVMouse.stroke();

          this.canvasCtxSVMouse.beginPath();
          this.canvasCtxSVMouse.arc(offset.x, offset.y, 6, 0, 2 * pi);
          this.canvasCtxSVMouse.strokeStyle = "white";
          this.canvasCtxSVMouse.lineWidth = 2;
          this.canvasCtxSVMouse.stroke();

          var imageData = (this.canvasCtxSV.getImageData(
              offset.x, offset.y, 1, 1) as ImageData).data;
          var prevAlpha = this.selectedColorRGB.a;
          this.selectedColorRGB = new Vector4(
              imageData[0] / 255,
              imageData[1] / 255,
              imageData[2] / 255,
              prevAlpha);

          break;
        case 1:
          this.canvasCtxHMouse.clearRect(
              0, 0, this.canvasWidthRectangle, this.canvasHeight);
          this.canvasCtxHMouse.fillStyle = "white";
          this.canvasCtxHMouse.fillRect(
              0, offset.y - 2, this.canvasWidthRectangle, 2);
          this.canvasCtxHMouse.fillRect(
              0, offset.y + 2, this.canvasWidthRectangle, 2);

          var imageData = (this.canvasCtxH.getImageData(
              offset.x, offset.y, 1, 1) as ImageData).data;
          this.selectedHueColor = new Vector4(
              imageData[0] / 255,
              imageData[1] / 255,
              imageData[2] / 255,
              imageData[3] / 255);

          Vector4 pickedColorHSV = new Vector4.zero();
          Colors.rgbToHsv(this.selectedHueColor, pickedColorHSV);

          Vector4 selectedColorHSV = new Vector4.zero();
          Colors.rgbToHsv(this.selectedColorRGB, selectedColorHSV);

          selectedColorHSV.r = pickedColorHSV.r;

          Colors.hsvToRgb(selectedColorHSV, this.selectedColorRGB);

          this.updateGradientMap();

          break;
        case 2:
          this.canvasCtxAMouse.clearRect(
              0, 0, this.canvasWidthRectangle, this.canvasHeight);
          this.canvasCtxAMouse.fillStyle = "white";
          this.canvasCtxAMouse.fillRect(
              0, offset.y - 2, this.canvasWidthRectangle, 2);
          this.canvasCtxAMouse.fillRect(
              0, offset.y + 2, this.canvasWidthRectangle, 2);

          var alpha = 1.0 - (offset.y / this.canvasHeight);
          this.selectedColorRGB.a = alpha;

          break;
        default: //DO nothing
          break;
      }
    }
  }

  void changeColorSelectState(MouseEvent event) {
    this.enableMouseTrack = event.type == "mousedown";

    if (this.enableMouseTrack) {
      switch ((event.target as CanvasElement).id) {
        case "canvasSVMouse":
          this.selectedGradient = 0;
          break;
        case "canvasHMouse":
          this.selectedGradient = 1;
          break;
        case "canvasAMouse":
          this.selectedGradient = 2;
          break;
        default:
          this.selectedGradient = -1;
          break;
      }
      this.selectColorFromGradient(event);
    } else {
      this.selectedGradient = -1;
    }
  }

  bool isUpperTabSelected(String name) {
    return name == this.selectedColorInputTab;
  }

  void updateColorDialogInput(int index) {
    this.selectedColorInputTab = this.tabLabelsInput[index];
  }

  bool isTabSelected(String name) {
    return name == this.selectedColorTab;
  }

  void selectTab(String selectedTab) {
    this.selectedColorTab = selectedTab;
    if (selectedTab != "HSL") {
      this.gradientSetupWasCompleted = false;
    }
  }

  void cancel() {
    visible = false;
    this._visibleStream.add(false);
    colorButtonInput.focus();
  }

  void selectColor(){
    this.colorHistory[toHexString(this.selectedColorRGB, alpha: true)] = this.selectedColorRGB.clone();
    this._lastlySelectedColorString = this.selectedColorString;
    cancel();
  }

  void clearSelectedColor(){
    this.selectedColorRGB = Colors.red;
    this.selectedColorHSV = new Vector4.zero();
    Colors.rgbToHsv(this.selectedColorRGB, this.selectedColorHSV);
    this.selectedHueColor = Colors.red;
    this._lastlySelectedColorString = "";
  }

  void open_dialog(MouseEvent event) {
    event.preventDefault();
    print(event);
    visible = true;
  }

  @override
  void ngAfterViewInit() {
    print(document.querySelector("#canvasSV"));
  }

  @override
  Future<Null> ngOnInit() async {
    //print(document.querySelector("material-tab-strip"));
  }

  @override
  void ngAfterViewChecked() {
    if (this.selectedColorTab == "HSL") {
      if (this.canvasCtxSV == null &&
          document.querySelector("#canvasSV") != null) {
        this.canvasCtxSV =
            (document.querySelector("#canvasSV") as CanvasElement).getContext(
                "2d");
      }
      if (this.canvasCtxH == null &&
          document.querySelector("#canvasH") != null) {
        this.canvasCtxH =
            (document.querySelector("#canvasH") as CanvasElement).getContext(
                "2d");
      }
      if (this.canvasCtxA == null &&
          document.querySelector("#canvasA") != null) {
        this.canvasCtxA =
            (document.querySelector("#canvasA") as CanvasElement).getContext(
                "2d");
      }

      if (this.canvasCtxSVMouse == null &&
          document.querySelector("#canvasSVMouse") != null) {
        this.canvasCtxSVMouse =
            (document.querySelector("#canvasSVMouse") as CanvasElement)
                .getContext("2d");
      }
      if (this.canvasCtxHMouse == null &&
          document.querySelector("#canvasHMouse") != null) {
        this.canvasCtxHMouse =
            (document.querySelector("#canvasHMouse") as CanvasElement)
                .getContext("2d");
      }
      if (this.canvasCtxAMouse == null &&
          document.querySelector("#canvasAMouse") != null) {
        this.canvasCtxAMouse =
            (document.querySelector("#canvasAMouse") as CanvasElement)
                .getContext("2d");
      }
      if (!this.gradientSetupWasCompleted && this.canvasCtxSV != null &&
          this.canvasCtxA != null && this.canvasCtxH != null) {
        this.setupGradient();
      }
    } else {
      this.canvasCtxH = this.canvasCtxA = this.canvasCtxSV = canvasCtxAMouse = canvasCtxSVMouse = canvasCtxHMouse = null;
    }
  }

  String toHexString(Vector4 input,
      {bool alpha: false, bool short: false}) {
    final int r = (input.r * 0xFF).floor() & 0xFF;
    final int g = (input.g * 0xFF).floor() & 0xFF;
    final int b = (input.b * 0xFF).floor() & 0xFF;
    final int a = (input.a * 0xFF).floor() & 0xFF;

    final bool isShort = short &&
        ((r >> 4) == (r & 0xF)) &&
        ((g >> 4) == (g & 0xF)) &&
        ((b >> 4) == (b & 0xF)) &&
        (!alpha || (a >> 4) == (a & 0xF));

    if (isShort) {
      final String rgb = (r & 0xF).toRadixString(16) +
          (g & 0xF).toRadixString(16) +
          (b & 0xF).toRadixString(16);

      return alpha ? (a & 0xF).toRadixString(16) + rgb : rgb;
    } else {
      final String rgb = r.toRadixString(16).padLeft(2, '0') +
          g.toRadixString(16).padLeft(2, '0') +
          b.toRadixString(16).padLeft(2, '0');

      return alpha ? rgb + a.toRadixString(16).padLeft(2, '0') : rgb;
    }
  }



}
